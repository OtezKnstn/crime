package com.example.crime

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.crime.databinding.FraqmentDialogBinding


class DialogFragment: DialogFragment() {

    lateinit var activityInterractor : ActivityInterractor
    private var _binding: FraqmentDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FraqmentDialogBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.btnChangeDateAndTime.setOnClickListener{
            DateAndTimeClass.date = binding.editTextDate.text.toString()
            DateAndTimeClass.time = binding.editTextTime.text.toString()

            Toast.makeText(context, "Дата и время изменены", Toast.LENGTH_LONG).show()

            dismiss()
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ActivityInterractor) {
            this.activityInterractor = context
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        activityInterractor.OnFragmentClosed()
    }
}