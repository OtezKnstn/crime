package com.example.crime

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.crime.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), ActivityInterractor {
    lateinit var binding: ActivityMainBinding
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.textCrime.text = "Преступление было совершенно ${DateAndTimeClass.date} в ${DateAndTimeClass.time}"
        binding.gotoChangeDateAndTime.setOnClickListener() {
            var dialog = DialogFragment()
            dialog.show(supportFragmentManager, "customDialog")
        }
    }

    override fun OnFragmentClosed() {
        binding.textCrime.text = "Преступление было совершенно ${DateAndTimeClass.date} в ${DateAndTimeClass.time}"
    }

}